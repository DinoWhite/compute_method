import math
'''
l1 = []
l2 = []

for i in range(11):
    l1.append(0.4*i*(1-0.1*i))


def prli(li):
    for i in li:
        print(i)


def up_tri(l_old):
    l_new = [0]
    for i in range(len(l_old)-2):
        l_new.append(l_old[i]/6+l_old[i+1]*2/3+l_old[i+2]/6)
    l_new.append(0)
    return l_new


for i in range(36):
    l1 = up_tri(l1)


prli(l1)
'''

'''
l1 = []
h = 0.2

for i in range(6):
    l1.append(4*h*i*(1-h*i))


def prli(li):
    for i in li:
        print(i)


def up_tri(l_old):
    l_new = [0]
    for i in range(len(l_old)-2):
        l_new.append(l_old[i]/6+l_old[i+1]*2/3+l_old[i+2]/6)
    l_new.append(0)
    return l_new


for i in range(2):
    l1 = up_tri(l1)
    prli(l1)
    print()

'''

l1 = []
h = 0.2

for i in range(6):
    l1.append(0 if math.sin(h*i*math.pi) <
              0.0000000001 else math.sin(h*i*math.pi))


def prli(li):
    for i in li:
        print(i)


def up_tri(l_old):
    l_new = []
    for i in range(len(l_old)):
        l_new.append(l_old[i]+h**2*i*(1-h*i))
    return l_new


def first_format(l_up, l_down):
    l_temp = l_up
    l_new = [0]
    for i in range(1, len(l_up)-1):
        l_new.append(l_up[i-1]+l_up[i+1]-l_down[i])
    l_new.append(0)
    return l_new, l_temp


print(l1, '\n')
l_down = up_tri(l1)
l_up = up_tri(l_down)
print(l_up, '\n')


for i in range(4):
    l_up, l_down = first_format(l_up, l_down)
    print(l_up, '\n')
