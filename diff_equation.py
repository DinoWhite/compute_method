import math


def eular(x, y, h):
    # return y + h * (y - 2 * x / y)
    return y + h * (2/(y-x)+1)


def adjust(x, y, y_1, h):
    # return y+h/2*(y-2*x/y + y_1-2*(x+h)/y_1)
    return y+h/2*(2/(y-x)+1 + 2/(y_1-(x+h))+1)


def eular_adjust(x, y, h):
    y_1 = eular(x, y, h)
    return adjust(x, y, y_1, h)


def trapezium(x, y, h):
    y1 = eular(x, y, h)
    y2 = adjust(x, y, y1, h)
    while abs(y2 - y1) > 0.0001:
        y1 = y2
        y2 = adjust(x, y, y1, h)
    return y2


def f(x):
    return -x-1+2*math.exp(x)


x = 0
y = 1
h = 0.1
# 欧拉法
'''
for i in range(10):
    y = eular(x, y, h)
    x += h
    print(y)
'''

# 欧拉-矫正
'''
for i in range(10):
    y = eular_adjust(x, y, h)
    x += h
    print(y)
'''

# 梯形法
'''
for i in range(10):
    y = trapezium(x, y, h)
    x += h
    print(y)

'''
print("梯形")

x = 0
y = 2
h = 0.2
n = int(1/h)
for i in range(n):
    y = trapezium(x, y, h)
    x += h
    print(y)


# print()
# x = 0
# y = 1
# h = 0.1

# print("欧拉矫正")
# for i in range(10):
    # y = eular_adjust(x, y, h)
    # x += h
    # print(y)

# print()
# for i in range(10):
    # print(f((i+1)/10))
