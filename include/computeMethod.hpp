#pragma once
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <vector>
#include <Eigen/Dense>

class computeMethod
{
public:
    computeMethod(double possibleRoot = 0);
    // 迭代
    double rec_Speedup(double x, double precision, bool print = 0); // 迭代加速
    double Aitken_Speedup(double x, double precision, bool print = 0); // aitken加速

    // 矩阵方程
    void DoolittleLU(Eigen::MatrixXd mat, Eigen::VectorXd& vec, bool print = 0); // LU分解
    void MatrixRec(Eigen::MatrixXd mat, Eigen::VectorXd& vec, double precision, int flag = 0, bool print = 0); // Jacobi迭代 和 Seidel迭代 默认Jacobi，1为Seidel

    // 插值方法
    double LagrangeInterpolation(std::vector<double> x, std::vector<double> f, double precision); // 拉格朗日插值
    double NewtonInterpolation(std::vector<double> x, std::vector<double> f, double precision); // 牛顿插值
    double NodeInterpolation(std::vector<double> x, std::vector<double> f, double precision); // 节点插值

    // 特征值
    double powerMethod(Eigen::MatrixXd mat, Eigen::VectorXd& vec, double precision, bool print = 0); // 乘幂法
    double powerMethodRev(Eigen::MatrixXd mat, Eigen::VectorXd& vec, double feature, double precision, bool print = 0); // 反幂法
    Eigen::MatrixXd JocabiMethod(Eigen::MatrixXd mat, double precision, bool print = 0); // 雅可比法
    Eigen::VectorXd eigenVector(Eigen::MatrixXd, double);

    // 最小二乘
    Eigen::VectorXd LeastSquare(Eigen::VectorXd x, Eigen::VectorXd f, int prime, bool print = 0);

    // 数值积分
    double NumericIntegration(Eigen::VectorXd x, Eigen::VectorXd f, int method = 0, bool print = 0);

    double m_possibleRoot;
    double m_xn1;
    double m_xn2;

private:
    std::vector<double> Diff_quotient(std::vector<double>, std::vector<double>); // 差商
    std::vector<double> Diff_method(std::vector<double>, std::vector<double>); // 差分
    double down4up5(double, int); // 4舍5入
    double factorial(int); // 阶乘
};
