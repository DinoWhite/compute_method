#include "../include/computeMethod.hpp"

//#define DOOLITTLE
//#define JACOBI
//#define LAGRANGE
//#define NEWTON
//#define NODE
//#define POWER
//#define POWERREV
//#define JACOBIMETHOD
//#define LEASTSQUARE
#define INTEGRATION

int main(int argc, char** argv)
{
    computeMethod rec_root(1.5);
    // 迭代加速测试
    //std::cout << rec_root.rec_Speedup(rec_root.m_possibleRoot, 0.00001) << "\n";
    //std::cout << rec_root.Aitken_Speedup(rec_root.m_possibleRoot, 0.00001) << "\n";
    //std::cout << "\n\n";

    //std::vector<double> solve;
    //std::vector<std::vector<double>> mat =
    //{
    //{10, -2, -1, 3},
    //{-2, 10, -1, 15},
    //{-1, -2, 5, 10}
    //};

#ifdef DOOLITTLE
    // 矩阵直接三角分解法测试
    Eigen::VectorXd solve(3, 1);
    solve.setZero();
    Eigen::MatrixXd mat(3, 4);
    //mat <<
    //10, -2, -1, 3,
    //-2, 10, -1, 15,
    //-1, -2, 5, 10;
    mat <<
        2, 1, 2, 6,
        4, 5, 4, 18,
        6, -3, 5, 5;
    rec_root.DoolittleLU(mat, solve, 1);
    std::cout << "LU分解 :\n";
    std::cout << solve;
    std::cout << "\n\n";
#endif

#ifdef JACOBI
    // Jacobi迭代
    Eigen::VectorXd solve(3, 1);
    solve.setZero();
    Eigen::MatrixXd mat(3, 4);
    mat <<
        10, -2, -1, 3,
        -2, 10, -1, 15,
        -1, -2, 5, 10;
    //rec_root.MatrixRec(mat, solve, 0.00001, 0, 1);
    //std::cout << "Jocabi迭代 : \n";
    //std::cout << solve;
    //std::cout << "\n\n";

    //solve.setZero();
    rec_root.MatrixRec(mat, solve, 0.001, 1, 1);
    std::cout << "Seidel迭代 : \n";
    std::cout << solve;
    std::cout << "\n\n";
#endif

#ifdef LAGRANGE
    // Lagrange插值
    std::vector<double> x{1.1275, 1.1503, 1.1735, 1.1972};
    std::vector<double> f{0.1191, 0.13954, 0.15932, 0.17903};
    std::cout << "lagrange插值 : " << rec_root.LagrangeInterpolation(x, f, 1.1300) << "\n\n";
#endif

#ifdef NEWTON
    // Newton插值
    std::vector<double> x1{0.40, 0.55, 0.65, 0.80, 0.90, 1.05};
    std::vector<double> f1{0.41075, 0.57815, 0.69675, 0.88811, 1.02652, 1.25386};
    std::cout << "Newton插值 : " << rec_root.NewtonInterpolation(x1, f1, 0.596) << "\n\n";
#endif

#ifdef NODE
    // Node插值
    std::vector<double> x2{0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80};
    std::vector<double> f2{0.521095, 0.578152, 0.636654, 0.696654, 0.758584, 0.822317, 0.888106};
    std::cout << "Node插值 : " << rec_root.NodeInterpolation(x2, f2, 0.52) << "\n\n";
#endif

#ifdef POWER
    // 乘幂法
    Eigen::MatrixXd ma(3, 3);
    Eigen::VectorXd vec(3, 1);
    double precision = 0.01;
    ma <<
       7, 3, -2,
       3, 4, -1,
       -2, -1, 3;
    vec << 0, 0, 1;
    double mk = rec_root.powerMethod(ma, vec, precision, 1);
    std::cout << "mk : " << mk << "\n vec is>\n" << vec << "\n\n";
#endif

#ifdef POWERREV
    // 反幂法
    Eigen::MatrixXd ma(3, 3);
    Eigen::VectorXd vec(3, 1);
    double precision = 0.00001;
    ma <<
       -12, 3, 3,
       3, 1, -2,
       3, -2, 7;
    vec << 1, 0, 0;
    double eigen = rec_root.powerMethodRev(ma, vec, -13, precision, 1);
    std::cout << "mk : " << eigen << "\n vec is ->\n" << vec << "\n\n";
#endif

#ifdef JACOBIMETHOD
    Eigen::MatrixXd ma(3, 3);
    Eigen::VectorXd vec(3, 1);
    double precision = 0.00001;
    ma <<
       2, -1, 0,
       -1, 2, -1,
       0, -1, 2;
    //ma << -12, 3, 3, 3, 1, -2, 3, -2, 7;
    Eigen::MatrixXd ret_mat = rec_root.JacabiMethod(ma, precision);
    std::cout << "\n vec is -> \n" << ret_mat << "\n\n";
    //std::cout << rec_root.eigenVector(ma, ret_mat(1, 1)) << "\n";
    //for (int i = 0; i < ret_mat.rows(); ++i)
    //{
    //vec << 1, 0, 0;
    //std::cout << "mat feature : " << ret_mat(i, i) << "\t";
    //rec_root.powerMethodRev(ma, vec, ret_mat(i, i), 0.0001, 1);
    //std::cout << "return vec is \n" << vec << "\n\n";
    //}
    //vec << 1, 0, 0;
    //rec_root.powerMethodRev(ma, vec, ret_mat(0, 0), 1);
    //std::cout << vec << "\n";
#endif

#ifdef LEASTSQUARE
    Eigen::VectorXd x(8);
    Eigen::VectorXd f(8);
    x << 1.36, 1.49, 1.73, 1.81, 1.95, 2.16, 2.28, 2.48;
    f << 14.094, 15.069, 16.844, 17.378, 18.435, 19.949, 20.963, 22.495;
    auto solve = rec_root.LeastSquare(x, f, 1, 1);
    std::cout << solve << "一次\n";
    solve = rec_root.LeastSquare(x, f, 2, 1);
    std::cout << solve << "二次\n";

    //Eigen::VectorXd x(7);
    //Eigen::VectorXd f(7);
    //x << 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8;
    //f << 3.16, 2.38, 1.75, 1.34, 1.00, 0.74, 0.56;
    //f = f.array().log();
    //std::cout << f << "\n\n";
    //auto solve = rec_root.LeastSquare(x, f, 1, 1);
    //std::cout << solve << "一次\n";

    //Eigen::MatrixXd ma(7, 2);
    //Eigen::VectorXd vec(7);
    //ma <<
    //1, 0.2,
    //1, 0.3,
    //1, 0.4,
    //1, 0.5,
    //1, 0.6,
    //1, 0.7,
    //1, 0.8;
    //vec << 3.16, 2.38, 1.75, 1.34, 1.00, 0.74, 0.56;
    //vec = vec.array().log();
    //vec = ma.transpose() * vec;
    //std::cout << vec << "\n";
    //ma = ma.transpose() * ma;
    //std::cout << "ma\n" << ma << "\n";

    //ma.conservativeResize(ma.rows(), ma.cols() + 1);
    //ma.topRightCorner(ma.rows(), 1) = vec;
    //std::cout << "vec\n" << vec << "\n\n";
    //rec_root.MatrixRec(ma, vec.setZero(), 0.00001, 1);
    //std::cout << vec << "\n";
#endif

#ifdef INTEGRATION
    Eigen::VectorXd x(9);
    Eigen::VectorXd f(9);
    x << 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0;
    f << 4, 3.938462, 3.764706, 3.506849, 3.2, 2.876405, 2.56, 2.265487, 2;

    //Eigen::VectorXd x(11);
    //Eigen::VectorXd f(11);
    //x << 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0;
    //f << 1, 1.004971, 1.019536, 1.042668, 1.072707, 1.107432, 1.144157, 1.759859, 1.211307, 1.235211, 1.248375;
    double solve = rec_root.NumericIntegration(x, f, 1, 1 );
    std::cout << solve << "\n";
#endif
    return 0;
}
