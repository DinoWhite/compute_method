#include "../include/computeMethod.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <cmath>
#include <unistd.h>

computeMethod::computeMethod(double possibleRoot) : m_possibleRoot(possibleRoot) {}

double computeMethod::rec_Speedup(double x, double precision, bool print)
{
    double temp;

    m_xn1 = x;
    temp = std::pow((m_xn1 * m_xn1 + 1), 1.0 / 3);
    m_xn2 = (20 * temp - 9 * m_xn1) / 11;

    if (print) std::cout << "迭代加速   x_n1 -> " << m_xn1 << "\t\t x_n2 -> " << m_xn2 << std::endl;
    if (std::fabs(m_xn2 - m_xn1) < precision) return m_xn2;
    else return rec_Speedup(m_xn2, precision);
}

double computeMethod::Aitken_Speedup(double x, double precision, bool print)
{
    double temp1, temp2;

    m_xn1 = x;
    temp1 = std::pow((m_xn1 * m_xn1 + 1), 1.0 / 3);
    temp2 = std::pow((temp1 * temp1 + 1), 1.0 / 3);
    m_xn2 = (temp2 * m_xn1 - temp1 * temp1) / (temp2 - 2 * temp1 + m_xn1);

    if (print) std::cout << "Aitken加速 x_n1 -> " << m_xn1 << "\t\t x_n2 -> " << m_xn2 << std::endl;
    if (std::fabs(m_xn2 - m_xn1) < precision) return m_xn2;
    else return Aitken_Speedup(m_xn2, precision);
}

// 矩阵分解
// Doolittle分解
void computeMethod::DoolittleLU(Eigen::MatrixXd mat, Eigen::VectorXd& solve, bool print)
{
    int n = mat.rows(); // 方阵行
    for (int row = 0; row < n; ++row) // LU分解开始
    {
        if (row == 0) // 先算单独算第一列
        {
            for (int i = 1; i < n; ++i)
                mat(i, 0) /= mat(0, 0); // 先将第一列除(1, 1)的元素
            continue;
        }

        // 第二行从这里开始, 这里代表先按行计算
        for (int col = row; col < n; ++col)
        {
            double cross_product = 0; // 向量积
            for (int i = row - 1; i >= 0; --i)
                cross_product += mat(i, col) * mat(row, i);
            mat(row, col) -= cross_product; // 将原矩阵元素减去左右最长向量的向量积，从最左和最上开始向该元素靠近算
        }

        // 按行计算完后按列计算
        for (int cur_row = row + 1; cur_row < n; ++cur_row)
        {
            int col = row;
            double cross_product = 0;
            for (int i = row - 1; i >= 0; --i)
                cross_product += mat(i, col) * mat(cur_row, i);
            mat(cur_row, col) = (mat(cur_row, col) - cross_product) / mat(row, row); // 将矩阵减去同上的向量积后再除同一列对角线元素
        }
    } // LU分解完成

    int max_col = mat.cols();
    // 解下三角阵
    for (int cur_row = 1; cur_row < n; ++cur_row) // 从第二行开始，第一行值因为是1所以不用算
    {
        double cross_product = 0;
        for (int i = cur_row - 1; i >= 0; --i)
            cross_product += mat(i, max_col - 1) * mat(cur_row, i);
        mat(cur_row, max_col - 1) -= cross_product;
    }

    mat(n - 1, max_col - 1) /= mat(n - 1, max_col - 2);
    // 解上三角阵，即最后解
    for (int cur_row = n - 2; cur_row >= 0; --cur_row)
    {
        double cross_product = 0;
        for (int i = 1; i <= (n - 1 - cur_row); ++i)
            cross_product += mat( n - i, max_col - 1) * mat(cur_row, max_col - 1 - i);
        mat(cur_row, max_col - 1) = (mat(cur_row, max_col - 1) - cross_product) / mat(cur_row, cur_row);
    }
    if (print) std::cout << "LU\n" << mat << "\n";

    for (int i = 0; i < n; ++i)
        solve(i, 0) = (mat(i, n));
}

void computeMethod::MatrixRec(Eigen::MatrixXd mat, Eigen::VectorXd& roots, double precision, int flag, bool print)
{
    int row = mat.rows();
    int col = mat.cols();
    if (row != roots.rows() || (row + 1) != col)
    {
        std::cout << "输入格式错误！！\n";
        return;
    }

    // 做成迭代式
    for (int i = 0; i < row; ++i)
    {
        double temp = mat(i, i);
        for (int j = 0; j < col; ++j)
        {
            mat(i, j) /= temp;
            if (i != j && j != col - 1)
                mat(i, j) = -mat(i, j);
        }
        mat(i, i) = roots(i, 0);
    }

    while (true)
    {
        double cur_err = 0;
        for (int i = 0; i < row; ++i)
        {
            double cross_product = 0;
            for (int j = 0; j < col - 1; ++j)
                if (i != j)
                    cross_product += mat(i, j) * mat(j, j);
            roots[i] = cross_product + mat(i, col - 1);
            if (flag)
            {
                double temp_err = cross_product + mat(i, col - 1) - mat(i, i);
                if (temp_err > cur_err)
                    cur_err = temp_err;
                mat(i, i) = roots(i, 0);
            }
        }

        if (!flag) // jacobi
        {
            for (int i = 0; i < row; ++i)
            {
                double temp_err = std::fabs(mat(i, i) - roots[i]);
                if (temp_err > cur_err)
                    cur_err = temp_err;
                mat(i, i) = roots(i, 0);
            }
        }
        if (print) std::cout << "\nstart\n" << mat << "\nend\n";
        if (cur_err < precision) break;
    }
}

// 插值
// Lagrange插值法
double computeMethod::LagrangeInterpolation(std::vector<double> x, std::vector<double> f, double x0)
{
    if (x.size() != f.size())
    {
        std::cout << "根与值数目不匹配!\n";
        return 0;
    }

    double answer = 0;
    for (int i = 0; i < x.size(); ++i)
    {
        double temp = 1;
        for (int j = 0; j < x.size(); ++j)
            if (i != j)
                temp *= (x0 - x[j]) / (x[i] - x[j]); // 插值基函数公式
        answer += temp * f[i];
    }
    return answer;
}

// Newton插值法
double computeMethod::NewtonInterpolation(std::vector<double> x, std::vector<double> f, double x0)
{
    if (x.size() != f.size())
    {
        std::cout << "根与值数目不匹配!\n";
        return 0;
    }

    std::vector<double> nodes = this->Diff_quotient(x, f); // 差商
    double answer = 0;
    for (int i = nodes.size() - 2; i >= 0; --i)
        answer += (answer + nodes[i + 1]) * (x0 - x[i]); // 公式，但是换了计算顺序，加速计算
    return answer + nodes[0];
}

std::vector<double> computeMethod::Diff_quotient(std::vector<double> x, std::vector<double> f)
{
    std::vector<double> ret;
    std::vector<double> temp;
    ret.emplace_back(f[0]); // 先放入第一项，即第一个x对应的f
    int count = 0;
    while (count < x.size() - 1) // 控制循环次数
    {
        for (int i = 0; i < x.size() - 1 - count; ++i)
            temp.emplace_back(down4up5((f[i + 1] - f[i]) / (x[i + 1 + count] - x[i]), 4)); // 每次间隔会增大一个，即count++
        ret.emplace_back(temp[0]); // 放入计算好的下一列的第一个元素
        f.clear();
        f = temp; // 保存刚算出来的一列
        temp.clear(); // 清空用来保存下一次计算的列
        ++count;
    }
    return ret;
}

double computeMethod::down4up5(double value, int precision)
{
    return std::floor((std::pow(10, precision) * value + 0.5)) / std::pow(10, precision);
}


// 节点插值
double computeMethod::NodeInterpolation(std::vector<double> x, std::vector<double> f, double x0)
{
    if (x.size() != f.size())
    {
        std::cout << "根与值数目不匹配!\n";
        return 0;
    }

    std::vector<double> nodes = this->Diff_method(x, f); // 差分
    double answer = 0;
    double t = (x0 - x[0]) / (x[1] - x[0]);
    for (int i = nodes.size() - 2; i >= 0; --i)
        answer += (answer + nodes[i + 1]) * (t - i) / factorial(i);
    return answer + nodes[0];
}

std::vector<double> computeMethod::Diff_method(std::vector<double> x, std::vector<double> f)
{
    std::vector<double> ret;
    ret.emplace_back(f[0]); // 放入第一个元素
    int count = 0;
    while (count < x.size() - 1)
    {
        for (int i = 0; i < x.size() - 1 - count; ++i)
            f[i] = f[i + 1] - f[i]; // 这里直接保存在同一向量即可，节省空间
        ret.emplace_back(f[0]);
        ++count;
    }
    return ret;
}

double computeMethod::factorial(int n)
{
    if (n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

// 乘幂法
double computeMethod::powerMethod(Eigen::MatrixXd mat, Eigen::VectorXd& vec, double precision, bool print)
{
    if (mat.cols() != mat.rows() || mat.isZero() || mat.cols() != vec.rows())
    {
        std::cout << "矩阵输入错误!\n";
        return 0;
    }

    double mk, mk_1;
    while (true)
    {
        mk_1 = mk;
        mk = 0;
        vec = mat * vec;
        for (int i = 0; i < vec.rows(); ++i)
            if (std::fabs(vec(i, 0)) > std::fabs(mk))
                mk = vec(i, 0); // 保存最大值用于归一化
        if (print) std::cout << "mk : " << mk << "\t误差 : " << mk_1 - mk << "\t向量\n" << vec << "\n";
        vec = vec / mk; // 归一化
        if (std::fabs(mk_1 - mk) < precision) break; // 计算误差，满足就退出
    }

    return mk;
}

// 反幂法
double computeMethod::powerMethodRev(Eigen::MatrixXd mat, Eigen::VectorXd& vec, double feature, double precision, bool print)
{
    if (mat.cols() != mat.rows() || mat.isZero() || mat.cols() != vec.rows())
    {
        std::cout << "矩阵输入错误!\n";
        return 0;
    }
    double mk, mk_1;
    double* tempMat = new double[mat.size()] {0}; // 数组转矩阵
    for (int i = 0; i < mat.size(); i += mat.cols() + 1)
        tempMat[i] = feature;
    Eigen::Map<Eigen::MatrixXd> p_mat(tempMat, mat.rows(), mat.cols());
    mat -= p_mat;

    while (true)
    {
        mk_1 = mk;
        mk = 0;
        vec = mat.inverse() * vec; // 左乘逆矩阵解出向量
        for (int i = 0; i < vec.rows(); ++i)
            if (std::fabs(vec(i, 0)) > std::fabs(mk))
                mk = vec(i, 0);
        vec /= mk;
        if (print) std::cout << "mk : " << mk << "\t误差 : " << mk_1 - mk << "\t向量\n" << vec << "\n";
        if (std::fabs(mk_1 - mk) < precision) break;
    }
    return 1 / mk + feature;
}

// 雅可比法
Eigen::MatrixXd computeMethod::JocabiMethod(Eigen::MatrixXd mat, double precision, bool print)
{
    if (mat.cols() != mat.rows() || mat.isZero())
    {
        std::cout << "矩阵输入错误!\n";
        return Eigen::MatrixXd();
    }
    bool flag = false;
    int zero_count = 0;

    while (true)
    {
        flag = false;
        zero_count = 0;
        for (int mi = 0; mi < mat.rows() - 1; ++mi)
        {
            for (int mj = mi + 1; mj < mat.cols(); ++mj)
            {
                if (mat(mi, mi) != 0 && mat(mj, mj) != 0 && mat(mi, mj) != 0) //选择合适的元素进行计算
                {
                    double tan2theta = 2 * mat(mi, mj) / (mat(mi, mi) - mat(mj, mj));
                    double c = 1 / tan2theta;
                    double tan1theta = -c + std::sqrt(1 + c * c);
                    double costheta = 1 / std::sqrt(1 + tan1theta * tan1theta);
                    Eigen::MatrixXd R(mat.rows(), mat.cols()); // 计算公式
                    R.setZero();
                    for (int i = 0; i < mat.rows(); ++i)
                        R(i, i) = 1; //对角线置1
                    R(mi, mi) = costheta;
                    R(mi, mj) = -tan1theta * costheta;
                    R(mj, mi) = tan1theta * costheta;
                    R(mj, mj) = costheta;
                    mat = R.transpose() * mat * R;
                    for (int i = 0; i < mat.rows(); ++i)
                        for (int j = 0; j < mat.cols(); ++j)
                            if (std::fabs(mat(i, j)) < 10e-8) //过小值置为0
                            {
                                mat(i, j) = 0;
                                ++zero_count;
                            }
                    if (print) std::cout << mat << "\n\n";
                    flag = true;
                    break;
                }
            }
            if (flag) break; // 多退出一个循环
        }
        if (zero_count == mat.size() - mat.rows()) break; // 除了对角元素剩下全是0的话退出
    }
    return mat;
}



Eigen::VectorXd computeMethod::eigenVector(Eigen::MatrixXd mat, double feature)
{
    Eigen::VectorXd vec(mat.rows(), 1);
    vec.setZero();
    for (int i = 0; i < mat.rows(); ++i)
        mat(i, i) = (mat(i, i) - feature) < 10e-8 ? 0 : (mat(i, i) - feature);
    Eigen::MatrixXd T(mat.rows(), mat.cols() + 1); // 增广矩阵
    T.topLeftCorner(mat.rows(), mat.cols()) = mat;
    T.topRightCorner(mat.rows(), 1) = vec;
    //Eigen::FullPivLU<Eigen::MatrixXd> lu(T);

    return vec;
}

// 最小二乘
Eigen::VectorXd computeMethod::LeastSquare(Eigen::VectorXd x, Eigen::VectorXd f, int prime, bool print)
{
    auto f_backup = f;
    if (x.rows() != f.rows())
    {
        std::cout << "向量输入错误!\n";
        return Eigen::VectorXd();
    }

    Eigen::MatrixXd A(x.rows(), 1);
    A.setOnes();

    for (int i = 0; i < prime; ++i)
    {
        A.conservativeResize(A.rows(), A.cols() + 1);
        A.topRightCorner(A.rows(), 1) = x.array().pow(i + 1);
    }

    auto B = A;
    f = A.transpose() * f;
    A = A.transpose() * A;

    A.conservativeResize(A.rows(), A.cols() + 1);
    A.topRightCorner(A.rows(), 1) = f;
    this->MatrixRec(A, f.setZero(), 0.0000001, 1);
    if (print)
    {
        x = B * f;
        std::cout << "==================\nstart\n最小二乘方程\n\n" << A << "\n\n";
        std::cout << "均方误差 : " << std::sqrt((x - f_backup).array().square().sum()) << "\n";
        std::cout << "最大偏差 : " << (x - f_backup).array().abs().maxCoeff() << "\n";
        std::cout << "\nend\n=================\n";
    }


    return f;
}

// 数值积分
double computeMethod::NumericIntegration(Eigen::VectorXd x, Eigen::VectorXd f, int method, bool print)
{
    if (x.size() != f.size())
    {
        std::cout << "长度不一致\n";
        return 0;
    }

    double solve = 0;
    for (int i = 0; i < x.size(); ++i)
    {
        if (print) std::cout << "solve : " << solve << "\n";
        if (i == x.size() - 1 || i == 0 )
        {
            if (method == 2) solve += 7 * f[i];
            else solve += f[i];
            continue;
        }

        switch (method)
        {
        case 0:
            solve += 2 * f[i];
            break;
        case 1:
            if (i % 2) solve += 4 * f[i];
            else solve += 2 * f[i];
            break;
        case 2:
            if (i % 2) solve += 32 * f[i];
            else if ((i / 2) % 2) solve += 12 * f[i];
            else solve += 14 * f[i];
            break;
        }
    }
    if (print) std::cout << "solve : " << solve << "\n";

    int n = 0;

    if (method == 0) n = 2 * (x.size() - 1);
    else if (method == 1) n = 3 * (x.size() - 1);
    else if (method == 2) n = 22.5 * (x.size() - 1);
    return solve / n;
}
